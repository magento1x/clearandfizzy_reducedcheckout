# Reduced Checkout Free Magento Extension by Gareth Price

## Reduced Checkout Free

Customise, reduce and replace your Magento checkout process with a simple plugin

## About Reduced Checkout

As a developer and business owner myself I was fed up with having to deal with  intentionally obfuscated Magento extensions, so took the route of developing an  extension based on the needs and wants of myself and my clients - the result was this  extension, clearly written and documented.

Magento is notoriously time consuming and costly to customise. The checkout is probably the worst part of all - a small error here can cost you big money.


## User Experience

If you only have one payment method and/or one shipping method you can now remove the payment step altogether - stop forcing customers to go through this pointless conversion killing step!

## Key Features

***** Shipping - One shipping method? Remove the shipping step altogether.   
***** Payment - One payment method? Remove this step also.   
***** Remove the Fax field and/or telephone field.   
***** Integration with 3rd-party payment and shipping extensions should be easy as this extension makes no modifications to the base templates.

## Upgrade to PRO?

If you like the free version, make sure you checkout [Reduced Checkout PRO][1]. The PRO edition includes the free features plus:

***** Login - Amazon style login - you

[1]: http://www.magentocommerce.com/magento-connect/reduced-checkout-pro-guest-converter.html

[Source from magecloud.net](https://www.magecloud.net/marketplace/extension/reduced-checkout-free/ "Permalink to Reduced Checkout Free Magento Extension by Gareth Price")